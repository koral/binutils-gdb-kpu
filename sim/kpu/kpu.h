#ifndef KPU_H
#define KPU_H

/* Copyright 2009-2017 Free Software Foundation, Inc.

   This file is part of the Kpu simulator.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.  */

#define GET_RA	((inst & RA_MASK) >> RA_LOW)
#define GET_RB	((inst & RB_MASK) >> RB_LOW)
#define GET_RC	((inst & RC_MASK) >> RC_LOW)

#define CPU     cpu->kpu_cpu

#define RA      CPU.regs[ra]
#define RB      CPU.regs[rb]
#define RC      CPU.regs[rc]

#define SA	CPU.spregs[IMM & 0x1]

#define IMM     imm

#define PC	   CPU.regs[31]
#define	SR         CPU.regs[30]
#define RETINTREG  CPU.regs[29]
#define RETREG     CPU.regs[28]
#define SP         CPU.regs[27]

#define SR_OVERFLOW_MASK	(1 << 0)
#define SR_GREATER_MASK		(1 << 1)
#define SR_EQUAL_MASK		(1 << 2)
#define SR_GREATER_ZERO_MASK	(1 << 3)
#define SR_INT_ENABLE_MASK	(1 << 4)
#define SR_INTVECT_IN_ROM_MASK	(1 << 5)

#define MEM_RD_BYTE(X)	sim_core_read_1 (cpu, 0, read_map, X)
#define MEM_RD_HALF(X)	sim_core_read_2 (cpu, 0, read_map, X)
#define MEM_RD_WORD(X)	sim_core_read_4 (cpu, 0, read_map, X)
#define MEM_RD_UBYTE(X) (ubyte) MEM_RD_BYTE(X)
#define MEM_RD_UHALF(X) (uhalf) MEM_RD_HALF(X)
#define MEM_RD_UWORD(X) (uword) MEM_RD_WORD(X)

#define MEM_WR_BYTE(X, D) sim_core_write_1 (cpu, 0, write_map, X, D)
#define MEM_WR_HALF(X, D) sim_core_write_2 (cpu, 0, write_map, X, D)
#define MEM_WR_WORD(X, D) sim_core_write_4 (cpu, 0, write_map, X, D)
// FIXME ??
#define MEM_WR_UBYTE(X, D) sim_core_write_1 (cpu, 0, write_map, X, D)
#define MEM_WR_UHALF(X, D) sim_core_write_2 (cpu, 0, write_map, X, D)


#define KPU_SEXT8(X)	((char) X)
#define KPU_SEXT16(X)	((short) X)


#define CARRY		carry
#define C_rd		((SR & 0x4) >> 2)
#define C_wr(D)		SR = (D ? SR | 0x80000004 : SR & 0x7FFFFFFB)

#define C_calc(X, Y, C)	((((uword)Y == MAX_WORD) && (C == 1)) ?		 \
			 1 :						 \
			 ((MAX_WORD - (uword)X) < ((uword)Y + C)))



#define DELAY_SLOT      delay_slot_enable = 1
#define BRANCH          branch_taken = 1

#define NUM_REGS 	32
#define INST_SIZE 	4

#define MAX_WORD	0xFFFFFFFF

typedef char		byte;
typedef short		half;
typedef int		word;
typedef unsigned char	ubyte;
typedef unsigned short	uhalf;
typedef unsigned int	uword;

#define SYS_read		0x30
#define SYS_write		0x38
#define SYS_open		0x40
#define SYS_close		0x48
#define SYS_creat		0x50
#define SYS_link		0x58
#define SYS_unlink		0x60
#define SYS_time		0x68
#define SYS_lseek		0x70
#define SYS_access		0x78
#define SYS_times		0x80
#define SYS_exit		0x88


#endif /* KPU_H */
