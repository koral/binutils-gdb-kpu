/* Copyright 2009-2017 Free Software Foundation, Inc.

   This file is part of the KPU simulator.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see <http://www.gnu.org/licenses/>.  */

#ifndef KPU_SIM_MAIN
#define KPU_SIM_MAIN

#include "kpu.h"
#include "sim-basics.h"
#include "sim-base.h"

/* The machine state.
   This state is maintained in host byte order.  The
   fetch/store register functions must translate between host
   byte order and the target processor byte order.
   Keeping this data in target byte order simplifies the register
   read/write functions.  Keeping this data in native order improves
   the performance of the simulator.  Simulation speed is deemed more
   important.  */

struct kpu_regset
{
  uword	          regs[32];		/* primary registers */
  int		  cycles;
  int		  insts;

};

struct _sim_cpu {
  struct kpu_regset kpu_cpu;
  short stopped;
  sim_cpu_base base;
};

struct sim_state {

  sim_cpu *cpu[MAX_NR_PROCESSORS];
  sim_state_base base;
};

#endif /* KPU_SIM_MAIN */
