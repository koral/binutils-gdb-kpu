/* KPU-specific support for 32-bit ELF

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
   Boston, MA 02110-1301, USA.  */


int dbg = 0;

#include "sysdep.h"
#include "bfd.h"
#include "bfdlink.h"
#include "libbfd.h"
#include "elf-bfd.h"
#include "elf/kpu.h"
#include <assert.h>

#define	USE_RELA	/* Only USE_REL is actually significant, but this is
			   here are a reminder...  */
#define INST_WORD_SIZE 4

static reloc_howto_type * kpu_elf_howto_table [(int) R_KPU_max];

static reloc_howto_type kpu_elf_howto_raw[] =
{
   /* This reloc does nothing.  */
   HOWTO (R_KPU_NONE,		/* Type.  */
          0,			/* Rightshift.  */
          3,			/* Size (0 = byte, 1 = short, 2 = long).  */
          0,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_dont,  /* Complain on overflow.  */
          NULL,                  /* Special Function.  */
          "R_KPU_NONE", 	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0,			/* Source Mask.  */
          0,			/* Dest Mask.  */
          FALSE),		/* PC relative offset?  */

   /* Not aligned 16 bit relocation.  */
   HOWTO (R_KPU_16_NA,     	/* Type.  */
          0,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_bitfield, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_16_NA",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0x0,			/* Source Mask.  */
          0xffff,		/* Dest Mask.  */
          FALSE), 		/* PC relative offset?  */

   /* Half word aligned 16 bit relocation.  */
   HOWTO (R_KPU_16_HA,     	/* Type.  */
          1,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_bitfield, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_16_HA",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0x0,			/* Source Mask.  */
          0xffff,		/* Dest Mask.  */
          FALSE), 		/* PC relative offset?  */

   /* Word aligned 16 bit relocation.  */
   HOWTO (R_KPU_16_WA,     	/* Type.  */
          2,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_bitfield, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_16_WA",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0x0,			/* Source Mask.  */
          0xffff,		/* Dest Mask.  */
          FALSE), 		/* PC relative offset?  */

   /* Not aligned 21 bit relocation.  */
   HOWTO (R_KPU_21_NA,     	/* Type.  */
          0,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_bitfield, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_21_NA",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0xffe00000,		/* Source Mask.  */
          0x1fffff,		/* Dest Mask.  */
          FALSE), 		/* PC relative offset?  */

   /* Word aligned 21 bit relocation.  */
   HOWTO (R_KPU_21_WA,     	/* Type.  */
          2,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_bitfield, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_21_WA",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0xffe00000,		/* Source Mask.  */
          0x1fffff,		/* Dest Mask.  */
          FALSE), 		/* PC relative offset?  */

   /* A standard PCREL 26 bit relocation.  */
   HOWTO (R_KPU_26_PCREL,	/* Type.  */
          2,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          26,			/* Bitsize.  */
          TRUE,			/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_signed, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_26_PCREL",   	/* Name.  */
          TRUE,			/* Partial Inplace.  */
          0x0,			/* Source Mask.  */
          0x3ffffff,		/* Dest Mask.  */
          4), 			/* PC relative offset?  */

   /* A standard 32 bit relocation.  */
   HOWTO (R_KPU_32,	/* Type.  */
          0,			/* Rightshift.  */
          2,			/* Size (0 = byte, 1 = short, 2 = long).  */
          32,			/* Bitsize.  */
          FALSE,		/* PC_relative.  */
          0,			/* Bitpos.  */
          complain_overflow_dont, /* Complain on overflow.  */
          bfd_elf_generic_reloc,/* Special Function.  */
          "R_KPU_32",   	/* Name.  */
          FALSE,		/* Partial Inplace.  */
          0x0,			/* Source Mask.  */
          0xffffffff,		/* Dest Mask.  */
          0), 			/* PC relative offset?  */


};

#ifndef NUM_ELEM
#define NUM_ELEM(a) (sizeof (a) / sizeof (a)[0])
#endif

/* Initialize the kpu_elf_howto_table, so that linear accesses can be done.  */

static void
kpu_elf_howto_init (void)
{
  unsigned int i;

  for (i = NUM_ELEM (kpu_elf_howto_raw); i--;)
    {
      unsigned int type;

      type = kpu_elf_howto_raw[i].type;

      BFD_ASSERT (type < NUM_ELEM (kpu_elf_howto_table));

      kpu_elf_howto_table [type] = & kpu_elf_howto_raw [i];
    }
}

static reloc_howto_type *
kpu_elf_reloc_type_lookup (bfd * abfd ATTRIBUTE_UNUSED,
                           bfd_reloc_code_real_type code)
{
  enum elf_kpu_reloc_type kpu_reloc = R_KPU_NONE;

  switch (code)
    {
    case BFD_RELOC_NONE:
      kpu_reloc = R_KPU_NONE;
      break;
    case BFD_RELOC_KPU_16_WA:
      kpu_reloc = R_KPU_16_WA;
      break;
    case BFD_RELOC_KPU_16_HA:
      kpu_reloc = R_KPU_16_HA;
      break;
    case BFD_RELOC_KPU_16_NA:
      kpu_reloc = R_KPU_16_NA;
      break;
    case BFD_RELOC_RVA:
      kpu_reloc = R_KPU_16_WA;
      break;
    case BFD_RELOC_KPU_21_NA:
      kpu_reloc = R_KPU_21_NA;
      break;
    case BFD_RELOC_KPU_21_WA:
      kpu_reloc = R_KPU_21_WA;
      break;
    case BFD_RELOC_KPU_26_PCREL:
      kpu_reloc = R_KPU_26_PCREL;
      break;
    case BFD_RELOC_32:
      kpu_reloc = R_KPU_32;
      break;
    default:
      return (reloc_howto_type *) NULL;
    }

  if (!kpu_elf_howto_table [R_KPU_16_WA])
    /* Initialize howto table if needed.  */
    kpu_elf_howto_init ();

  return kpu_elf_howto_table [(int) kpu_reloc];
};

static reloc_howto_type *
kpu_elf_reloc_name_lookup (bfd *abfd ATTRIBUTE_UNUSED,
				  const char *r_name)
{
  unsigned int i;

  for (i = 0; i < NUM_ELEM (kpu_elf_howto_raw); i++)
    if (kpu_elf_howto_raw[i].name != NULL
	&& strcasecmp (kpu_elf_howto_raw[i].name, r_name) == 0)
      return &kpu_elf_howto_raw[i];

  return NULL;
}

/* Set the howto pointer for a RCE ELF reloc.  */

static void
kpu_elf_info_to_howto (bfd * abfd ATTRIBUTE_UNUSED,
			      arelent * cache_ptr,
			      Elf_Internal_Rela * dst)
{
  unsigned int r_type;

  if (!kpu_elf_howto_table [R_KPU_16_WA])
    /* Initialize howto table if needed.  */
    kpu_elf_howto_init ();

  r_type = ELF32_R_TYPE (dst->r_info);
  if (r_type >= R_KPU_max)
    {
      /* xgettext:c-format */
      _bfd_error_handler (_("%B: unrecognised Kpu reloc number: %d"),
			  abfd, r_type);
      bfd_set_error (bfd_error_bad_value);
      r_type = R_KPU_NONE;
    }

  cache_ptr->howto = kpu_elf_howto_table [r_type];
}

/* Kpu ELF local labels start with 'L.' or '$L', not '.L'.  */

static bfd_boolean
kpu_elf_is_local_label_name (bfd *abfd, const char *name)
{
  if (name[0] == 'L' && name[1] == '.')
    return TRUE;

  if (name[0] == '$' && name[1] == 'L')
    return TRUE;

  /* With gcc, the labels go back to starting with '.', so we accept
     the generic ELF local label syntax as well.  */
  return _bfd_elf_is_local_label_name (abfd, name);
}

/* The kpu linker (like many others) needs to keep track of
   the number of relocs that it decides to copy as dynamic relocs in
   check_relocs for each symbol. This is so that it can later discard
   them if they are found to be unnecessary.  We store the information
   in a field extending the regular ELF linker hash table.  */

struct elf32_mb_dyn_relocs
{
  struct elf32_mb_dyn_relocs *next;

  /* The input section of the reloc.  */
  asection *sec;

  /* Total number of relocs copied for the input section.  */
  bfd_size_type count;

  /* Number of pc-relative relocs copied for the input section.  */
  bfd_size_type pc_count;
};

/* ELF linker hash entry.  */

struct elf32_mb_link_hash_entry
{
  struct elf_link_hash_entry elf;

  /* Track dynamic relocs copied for this symbol.  */
  struct elf32_mb_dyn_relocs *dyn_relocs;

};

#define IS_TLS_GD(x)     (x == (TLS_TLS | TLS_GD))
#define IS_TLS_LD(x)     (x == (TLS_TLS | TLS_LD))
#define IS_TLS_DTPREL(x) (x == (TLS_TLS | TLS_DTPREL))
#define IS_TLS_NONE(x)   (x == 0)

#define elf32_mb_hash_entry(ent) ((struct elf32_mb_link_hash_entry *)(ent))

/* ELF linker hash table.  */

struct elf32_mb_link_hash_table
{
  struct elf_link_hash_table elf;

  /* Small local sym to section mapping cache.  */
  struct sym_cache sym_sec;

  /* TLS Local Dynamic GOT Entry */
  union {
    bfd_signed_vma refcount;
    bfd_vma offset;
  } tlsld_got;
};

/* Nonzero if this section has TLS related relocations.  */
#define has_tls_reloc sec_flg0

/* Get the ELF linker hash table from a link_info structure.  */

#define elf32_mb_hash_table(p)				\
  (elf_hash_table_id ((struct elf_link_hash_table *) ((p)->hash)) \
  == KPU_ELF_DATA ? ((struct elf32_mb_link_hash_table *) ((p)->hash)) : NULL)

/* Create an entry in a kpu ELF linker hash table.  */

static struct bfd_hash_entry *
link_hash_newfunc (struct bfd_hash_entry *entry,
		   struct bfd_hash_table *table,
		   const char *string)
{
  /* Allocate the structure if it has not already been allocated by a
     subclass.  */
  if (entry == NULL)
    {
      entry = bfd_hash_allocate (table,
				 sizeof (struct elf32_mb_link_hash_entry));
      if (entry == NULL)
	return entry;
    }

  /* Call the allocation method of the superclass.  */
  entry = _bfd_elf_link_hash_newfunc (entry, table, string);
  if (entry != NULL)
    {
      struct elf32_mb_link_hash_entry *eh;

      eh = (struct elf32_mb_link_hash_entry *) entry;
      eh->dyn_relocs = NULL;
    }

  return entry;
}

/* Create a mb ELF linker hash table.  */

static struct bfd_link_hash_table *
kpu_elf_link_hash_table_create (bfd *abfd)
{
  struct elf32_mb_link_hash_table *ret;
  bfd_size_type amt = sizeof (struct elf32_mb_link_hash_table);

  ret = (struct elf32_mb_link_hash_table *) bfd_zmalloc (amt);
  if (ret == NULL)
    return NULL;

  if (!_bfd_elf_link_hash_table_init (&ret->elf, abfd, link_hash_newfunc,
				      sizeof (struct elf32_mb_link_hash_entry),
				      KPU_ELF_DATA))
    {
      free (ret);
      return NULL;
    }

  return &ret->elf.root;
}

/* This code is taken from elf32-m32r.c
   There is some attempt to make this function usable for many architectures,
   both USE_REL and USE_RELA ['twould be nice if such a critter existed],
   if only to serve as a learning tool.

   The RELOCATE_SECTION function is called by the new ELF backend linker
   to handle the relocations for a section.

   The relocs are always passed as Rela structures; if the section
   actually uses Rel structures, the r_addend field will always be
   zero.

   This function is responsible for adjust the section contents as
   necessary, and (if using Rela relocs and generating a
   relocatable output file) adjusting the reloc addend as
   necessary.

   This function does not have to worry about setting the reloc
   address or the reloc symbol index.

   LOCAL_SYMS is a pointer to the swapped in local symbols.

   LOCAL_SECTIONS is an array giving the section in the input file
   corresponding to the st_shndx field of each local symbol.

   The global hash table entry for the global symbols can be found
   via elf_sym_hashes (input_bfd).

   When generating relocatable output, this function must handle
   STB_LOCAL/STT_SECTION symbols specially.  The output symbol is
   going to be the section symbol corresponding to the output
   section, which means that the addend must be adjusted
   accordingly.  */

static bfd_boolean
kpu_elf_relocate_section (bfd *output_bfd,
                          struct bfd_link_info *info,
                          bfd *input_bfd,
                          asection *input_section,
                          bfd_byte *contents,
                          Elf_Internal_Rela *relocs,
                          Elf_Internal_Sym *local_syms,
                          asection **local_sections)
{
  struct elf32_mb_link_hash_table *htab;
  Elf_Internal_Shdr *symtab_hdr = &elf_tdata (input_bfd)->symtab_hdr;
  struct elf_link_hash_entry **sym_hashes = elf_sym_hashes (input_bfd);
  Elf_Internal_Rela *rel, *relend;
  /* Assume success.  */
  bfd_boolean ret = TRUE;
  asection *sreloc ATTRIBUTE_UNUSED;

  if (!kpu_elf_howto_table[R_KPU_max-1])
    kpu_elf_howto_init ();

  htab = elf32_mb_hash_table (info);
  if (htab == NULL)
    return FALSE;

  sreloc = elf_section_data (input_section)->sreloc;

  rel = relocs;
  relend = relocs + input_section->reloc_count;
  for (; rel < relend; rel++)
    {
      int r_type;
      reloc_howto_type *howto;
      unsigned long r_symndx;
      bfd_vma addend = rel->r_addend;
      bfd_vma offset = rel->r_offset;
      struct elf_link_hash_entry *h;
      Elf_Internal_Sym *sym;
      asection *sec;
      bfd_reloc_status_type r = bfd_reloc_ok;
      const char *errmsg = NULL;
      bfd_boolean unresolved_reloc = FALSE;

      h = NULL;
      r_type = ELF32_R_TYPE (rel->r_info);

      if (r_type < 0 || r_type >= (int) R_KPU_max)
	{
	  /* xgettext:c-format */
	  _bfd_error_handler (_("%B: unknown relocation type %d"),
			      input_bfd, (int) r_type);
	  bfd_set_error (bfd_error_bad_value);
	  ret = FALSE;
	  continue;
	}

      howto = kpu_elf_howto_table[r_type];
      r_symndx = ELF32_R_SYM (rel->r_info);

      if (bfd_link_relocatable (info))
	{
	  /* This is a relocatable link.  We don't have to change
	     anything, unless the reloc is against a section symbol,
	     in which case we have to adjust according to where the
	     section symbol winds up in the output section.  */
	  sec = NULL;
	  if (r_symndx >= symtab_hdr->sh_info)
	    /* External symbol.  */
	    continue;

	  /* Local symbol.  */
	  sym = local_syms + r_symndx;
	  /* STT_SECTION: symbol is associated with a section.  */
	  if (ELF_ST_TYPE (sym->st_info) != STT_SECTION)
	    /* Symbol isn't associated with a section.  Nothing to do.  */
	    continue;

	  sec = local_sections[r_symndx];
	  addend += sec->output_offset + sym->st_value;
#ifndef USE_REL
	  /* This can't be done for USE_REL because it doesn't mean anything
	     and elf_link_input_bfd asserts this stays zero.  */
	  /* rel->r_addend = addend; */
#endif

#ifndef USE_REL
	  /* Addends are stored with relocs.  We're done.  */
	  continue;
#else /* USE_REL */
	  /* If partial_inplace, we need to store any additional addend
	     back in the section.  */
	  if (!howto->partial_inplace)
	    continue;
	  /* ??? Here is a nice place to call a special_function like handler.  */
	  r = _bfd_relocate_contents (howto, input_bfd, addend,
				      contents + offset);
#endif /* USE_REL */
	}
      else
	{
	  bfd_vma relocation;

	  /* This is a final link.  */
	  sym = NULL;
	  sec = NULL;
	  unresolved_reloc = FALSE;

	  if (r_symndx < symtab_hdr->sh_info)
	    {
	      /* Local symbol.  */
              if (howto->pc_relative)
                /* no need to adjust a pcrel reloc against a local sym */
                continue;
	      sym = local_syms + r_symndx;
	      sec = local_sections[r_symndx];
	      if (sec == 0)
		continue;
              relocation = _bfd_elf_rela_local_sym (output_bfd, sym, &sec, rel);
	      /* r_addend may have changed if the reference section was
		 a merge section.  */
	      addend = rel->r_addend;
	    }
	  else
	    {
	      /* External symbol.  */
	      bfd_boolean warned ATTRIBUTE_UNUSED;
	      bfd_boolean ignored ATTRIBUTE_UNUSED;

	      RELOC_FOR_GLOBAL_SYMBOL (info, input_bfd, input_section, rel,
				       r_symndx, symtab_hdr, sym_hashes,
				       h, sec, relocation,
				       unresolved_reloc, warned, ignored);
	    }

	  /* Sanity check the address.  */
	  if (offset > bfd_get_section_limit (input_bfd, input_section))
	    {
	      r = bfd_reloc_outofrange;
	      goto check_reloc;
	    }

	  switch ((int) r_type)
	    {
            case R_KPU_NONE:
              break;
	    case R_KPU_16_WA:
            case R_KPU_16_HA:
            case R_KPU_16_NA:
            case R_KPU_21_NA:
            case R_KPU_21_WA:
	    case R_KPU_32:
              r = _bfd_final_link_relocate (howto, input_bfd, input_section,
                                            contents, offset,
					    relocation, addend);
              break;
            case R_KPU_26_PCREL:
              r = _bfd_final_link_relocate (howto, input_bfd, input_section,
					    contents, offset,
					    relocation, addend - 1);
              /* unsigned int inst = bfd_get_32 (input_bfd, contents + offset); */
              /* relocation -= (input_section->output_section->vma */
              /*                + input_section->output_offset */
              /*                + offset + INST_WORD_SIZE) >> ; */
              /* inst = (inst & ~howto->dst_mask) | (relocation & howto->dst_mask); */
              /* bfd_put_32 (input_bfd, inst, contents + offset); */
              break;
            default:
              r = bfd_reloc_notsupported;
              break;
	    }
	}

    check_reloc:

      if (r != bfd_reloc_ok)
	{
	  /* FIXME: This should be generic enough to go in a utility.  */
	  const char *name;

	  if (h != NULL)
	    name = h->root.root.string;
	  else
	    {
	      name = (bfd_elf_string_from_elf_section
		      (input_bfd, symtab_hdr->sh_link, sym->st_name));
	      if (name == NULL || *name == '\0')
		name = bfd_section_name (input_bfd, sec);
	    }

	  if (errmsg != NULL)
	    goto common_error;

	  switch (r)
	    {
	    case bfd_reloc_overflow:
	      (*info->callbacks->reloc_overflow)
		(info, (h ? &h->root : NULL), name, howto->name,
		 (bfd_vma) 0, input_bfd, input_section, offset);
	      break;

	    case bfd_reloc_undefined:
	      (*info->callbacks->undefined_symbol)
		(info, name, input_bfd, input_section, offset, TRUE);
	      break;

	    case bfd_reloc_outofrange:
	      errmsg = _("internal error: out of range error");
	      goto common_error;

	    case bfd_reloc_notsupported:
	      errmsg = _("internal error: unsupported relocation error");
	      goto common_error;

	    case bfd_reloc_dangerous:
	      errmsg = _("internal error: dangerous error");
	      goto common_error;

	    default:
	      errmsg = _("internal error: unknown error");
	      /* Fall through.  */
	    common_error:
	      (*info->callbacks->warning) (info, errmsg, name, input_bfd,
					   input_section, offset);
	      break;
	    }
	}
    }

  return ret;
}

/* Copy the extra info we tack onto an elf_link_hash_entry.  */

static void
kpu_elf_copy_indirect_symbol (struct bfd_link_info *info,
				     struct elf_link_hash_entry *dir,
				     struct elf_link_hash_entry *ind)
{
  struct elf32_mb_link_hash_entry *edir, *eind;

  edir = (struct elf32_mb_link_hash_entry *) dir;
  eind = (struct elf32_mb_link_hash_entry *) ind;

  if (eind->dyn_relocs != NULL)
    {
      if (edir->dyn_relocs != NULL)
	{
	  struct elf32_mb_dyn_relocs **pp;
	  struct elf32_mb_dyn_relocs *p;

	  if (ind->root.type == bfd_link_hash_indirect)
	    abort ();

	  /* Add reloc counts against the weak sym to the strong sym
	     list.  Merge any entries against the same section.  */
	  for (pp = &eind->dyn_relocs; (p = *pp) != NULL; )
	    {
	      struct elf32_mb_dyn_relocs *q;

	      for (q = edir->dyn_relocs; q != NULL; q = q->next)
		if (q->sec == p->sec)
		  {
		    q->pc_count += p->pc_count;
		    q->count += p->count;
		    *pp = p->next;
		    break;
		  }
	      if (q == NULL)
		pp = &p->next;
	    }
	  *pp = edir->dyn_relocs;
	}

      edir->dyn_relocs = eind->dyn_relocs;
      eind->dyn_relocs = NULL;
    }

  _bfd_elf_link_hash_copy_indirect (info, dir, ind);
}

#define TARGET_LITTLE_SYM      kpu_elf32_le_vec
#define TARGET_LITTLE_NAME     "elf32-kpuel"

#define TARGET_BIG_SYM          kpu_elf32_vec
#define TARGET_BIG_NAME		"elf32-kpu"

#define ELF_ARCH		bfd_arch_kpu
#define ELF_TARGET_ID		KPU_ELF_DATA
#define ELF_MACHINE_CODE	EM_KPU
#define ELF_MAXPAGESIZE		0x1000
#define elf_info_to_howto	kpu_elf_info_to_howto
#define elf_info_to_howto_rel	NULL

#define bfd_elf32_bfd_reloc_type_lookup		kpu_elf_reloc_type_lookup
#define bfd_elf32_bfd_is_local_label_name       kpu_elf_is_local_label_name
#define elf_backend_relocate_section		kpu_elf_relocate_section
#define bfd_elf32_bfd_merge_private_bfd_data    _bfd_generic_verify_endian_match
#define bfd_elf32_bfd_reloc_name_lookup		kpu_elf_reloc_name_lookup

#define elf_backend_copy_indirect_symbol        kpu_elf_copy_indirect_symbol
#define bfd_elf32_bfd_link_hash_table_create    kpu_elf_link_hash_table_create
#define elf_backend_can_gc_sections		1
#define elf_backend_can_refcount    		1
#define elf_backend_want_dynrelro		1

#include "elf32-target.h"
