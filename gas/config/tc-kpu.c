/* tc-kpu.c -- Assemble code for KPU

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of GAS, the GNU Assembler.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GAS; see the file COPYING.  If not, write to the Free
   Software Foundation, 51 Franklin Street - Fifth Floor, Boston, MA
   02110-1301, USA.  */

#include "as.h"
#include <stdio.h>
#include "bfd.h"
#include "subsegs.h"
#define DEFINE_TABLE
#include "../opcodes/kpu-dis.h"
#include "safe-ctype.h"
#include <string.h>
#include <dwarf2dbg.h>
#include "aout/stab_gnu.h"

#ifndef streq
#define streq(a,b) (strcmp (a, b) == 0)
#endif

#define OPTION_EB (OPTION_MD_BASE + 0)
#define OPTION_EL (OPTION_MD_BASE + 1)

void kpu_generate_symbol (char *sym);

/* Several places in this file insert raw instructions into the
   object. They should generate the instruction
   and then use these four macros to crack the instruction value into
   the appropriate byte values.  */
#define	INST_BYTE0(x)  (target_big_endian ? (((x) >> 24) & 0xFF) : ((x) & 0xFF))
#define	INST_BYTE1(x)  (target_big_endian ? (((x) >> 16) & 0xFF) : (((x) >> 8) & 0xFF))
#define	INST_BYTE2(x)  (target_big_endian ? (((x) >> 8) & 0xFF) : (((x) >> 16) & 0xFF))
#define	INST_BYTE3(x)  (target_big_endian ? ((x) & 0xFF) : (((x) >> 24) & 0xFF))

/* This array holds the chars that always start a comment.  If the
   pre-processor is disabled, these aren't very useful.  */
const char comment_chars[] = ";";

const char line_separator_chars[] = "@";

/* This array holds the chars that only start a comment at the beginning of
   a line.  */
const char line_comment_chars[] = "#";

const int md_reloc_size = 8; /* Size of relocation record.  */

/* Chars that can be used to separate mant
   from exp in floating point numbers.  */
const char EXP_CHARS[] = "eE";

/* Chars that mean this number is a floating point constant
   As in 0f12.456
   or    0d1.2345e12.  */
const char FLT_CHARS[] = "rRsSfFdDxXpP";

/* INST_PC_OFFSET and INST_NO_OFFSET are 0 and 1.  */
#define UNDEFINED_PC_OFFSET  1
#define DEFINED_ABS_SEGMENT  2
#define UNDEFINED_ABS_SEGMENT 3
#define DEFINED_PC_OFFSET    4

/* Initialize the relax table.  */
const relax_typeS md_relax_table[] =
{
  { 0x7fffffff, 0x80000000, INST_WORD_SIZE*2, 0 },  /*  1: UNDEFINED_PC_OFFSET.  */
  { 0x7fffffff, 0x80000000, INST_WORD_SIZE*2, 0 },  /*  2: .  */
  { 0x7fffffff, 0x80000000, INST_WORD_SIZE*2, 0 },  /*  3: .  */
  { 0x7fffffff, 0x80000000, INST_WORD_SIZE*2, 0 },  /*  4: .  */
  { 0x7fffffff, 0x80000000, INST_WORD_SIZE*2, 0 },  /*  5: .  */
};

struct hash_control * opcode_hash_control;	/* Opcode mnemonics.  */

static segT sbss_segment = 0; 	/* Small bss section.  */
static segT rodata_segment = 0; /* read-only section.  */

/* Generate a symbol for stabs information.  */

void
kpu_generate_symbol (char *sym)
{
#define KPU_FAKE_LABEL_NAME "XL0\001"
  static int kpu_label_count;
  sprintf (sym, "%sL%d", KPU_FAKE_LABEL_NAME, kpu_label_count);
  ++kpu_label_count;
}

/* Handle the section changing pseudo-ops. */

static void
kpu_s_text (int ignore ATTRIBUTE_UNUSED)
{
#ifdef OBJ_ELF
  obj_elf_text (ignore);
#else
  s_text (ignore);
#endif
}

static int32_t
endianness_fix(int32_t x)
{
  if (target_big_endian)
    return __builtin_bswap32 (x);
  else
    return x;
}


/* If a .word, et. al., pseud-op is seen, warn if the value is not
   aligned correctly.  Note that this can cause warnings to be issued
   when assembling initialized structured which were declared with the
   packed attribute.  FIXME: Perhaps we should require an option to
   enable this warning?  */

void
kpu_cons_align (int nbytes)
{
  /* int nalign; */

  nbytes = nbytes;

  /* nalign = 0; */
  /* while ((nbytes & 1) == 0) */
  /*   { */
  /*     ++nalign; */
  /*     nbytes >>= 1; */
  /*   } */

  /* if (nalign == 0) */
  /*   return; */

  /* frag_var (rs_align_test, 1, 1, (relax_substateT) 0, */
  /* 	    (symbolS *) NULL, (offsetT) nalign, (char *) NULL); */

  /* record_alignment (now_seg, nalign); */
}

static void
kpu_s_data (int ignore ATTRIBUTE_UNUSED)
{
#ifdef OBJ_ELF
  obj_elf_change_section (".data", SHT_PROGBITS, 0, SHF_ALLOC+SHF_WRITE,
			  0, 0, 0, 0);
#else
  s_data (ignore);
#endif
}

/* Things in the .sdata segment are always considered to be in the small data section.  */

static void
kpu_s_sdata (int ignore ATTRIBUTE_UNUSED)
{
#ifdef OBJ_ELF
  obj_elf_change_section (".sdata", SHT_PROGBITS, 0, SHF_ALLOC+SHF_WRITE,
			  0, 0, 0, 0);
#else
  s_data (ignore);
#endif
}

/* Pseudo op to make file scope bss items.  */

static void
kpu_s_lcomm (int xxx ATTRIBUTE_UNUSED)
{
  char *name;
  char c;
  char *p;
  offsetT size;
  symbolS *symbolP;
  offsetT align;
  char *pfrag;
  int align2;
  segT current_seg = now_seg;
  subsegT current_subseg = now_subseg;

  c = get_symbol_name (&name);

  /* Just after name is now '\0'.  */
  p = input_line_pointer;
  (void) restore_line_pointer (c);
  SKIP_WHITESPACE ();
  if (*input_line_pointer != ',')
    {
      as_bad (_("Expected comma after symbol-name: rest of line ignored."));
      ignore_rest_of_line ();
      return;
    }

  input_line_pointer++;		/* skip ',' */
  if ((size = get_absolute_expression ()) < 0)
    {
      as_warn (_(".COMMon length (%ld.) <0! Ignored."), (long) size);
      ignore_rest_of_line ();
      return;
    }

  /* The third argument to .lcomm is the alignment.  */
  if (*input_line_pointer != ',')
    align = 8;
  else
    {
      ++input_line_pointer;
      align = get_absolute_expression ();
      if (align <= 0)
	{
	  as_warn (_("ignoring bad alignment"));
	  align = 8;
	}
    }

  *p = 0;
  symbolP = symbol_find_or_make (name);
  *p = c;

  if (S_IS_DEFINED (symbolP) && ! S_IS_COMMON (symbolP))
    {
      as_bad (_("Ignoring attempt to re-define symbol `%s'."),
	      S_GET_NAME (symbolP));
      ignore_rest_of_line ();
      return;
    }

  if (S_GET_VALUE (symbolP) && S_GET_VALUE (symbolP) != (valueT) size)
    {
      as_bad (_("Length of .lcomm \"%s\" is already %ld. Not changed to %ld."),
	      S_GET_NAME (symbolP),
	      (long) S_GET_VALUE (symbolP),
	      (long) size);

      ignore_rest_of_line ();
      return;
    }

  /* Allocate_bss.  */
  if (align)
    {
      /* Convert to a power of 2 alignment.  */
      for (align2 = 0; (align & 1) == 0; align >>= 1, ++align2);
      if (align != 1)
	{
	  as_bad (_("Common alignment not a power of 2"));
	  ignore_rest_of_line ();
	  return;
	}
    }
  else
    align2 = 0;

  record_alignment (current_seg, align2);
  subseg_set (current_seg, current_subseg);
  if (align2)
    frag_align (align2, 0, 0);
  if (S_GET_SEGMENT (symbolP) == current_seg)
    symbol_get_frag (symbolP)->fr_symbol = 0;
  symbol_set_frag (symbolP, frag_now);
  pfrag = frag_var (rs_org, 1, 1, (relax_substateT) 0, symbolP, size,
		    (char *) 0);
  *pfrag = 0;
  S_SET_SIZE (symbolP, size);
  S_SET_SEGMENT (symbolP, current_seg);
  subseg_set (current_seg, current_subseg);
  demand_empty_rest_of_line ();
}

static void
kpu_s_rdata (int localvar)
{
#ifdef OBJ_ELF
  if (localvar == 0)
    {
      /* rodata.  */
      obj_elf_change_section (".rodata", SHT_PROGBITS, 0, SHF_ALLOC,
			      0, 0, 0, 0);
      if (rodata_segment == 0)
	rodata_segment = subseg_new (".rodata", 0);
    }
  else
    {
      /* 1 .sdata2.  */
      obj_elf_change_section (".sdata2", SHT_PROGBITS, 0, SHF_ALLOC,
                              0, 0, 0, 0);
    }
#else
  s_data (ignore);
#endif
}

static void
kpu_s_bss (int localvar)
{
#ifdef OBJ_ELF
  if (localvar == 0) /* bss.  */
    obj_elf_change_section (".bss", SHT_NOBITS, 0, SHF_ALLOC+SHF_WRITE,
                            0, 0, 0, 0);
  else if (localvar == 1)
    {
      /* sbss.  */
      obj_elf_change_section (".sbss", SHT_NOBITS, 0,
                              SHF_ALLOC+SHF_WRITE, 0, 0, 0, 0);
      if (sbss_segment == 0)
	sbss_segment = subseg_new (".sbss", 0);
    }
#else
  s_data (ignore);
#endif
}

/* endp_p is always 1 as this func is called only for .end <funcname>
   This func consumes the <funcname> and calls regular processing
   s_func(1) with arg 1 (1 for end). */

static void
kpu_s_func (int end_p ATTRIBUTE_UNUSED)
{
  char *name;
  restore_line_pointer (get_symbol_name (&name));
  s_func (1);
}

/* Handle the .weakext pseudo-op as defined in Kane and Heinrich.  */

static void
kpu_s_weakext (int ignore ATTRIBUTE_UNUSED)
{
  char *name;
  int c;
  symbolS *symbolP;
  expressionS exp;

  c = get_symbol_name (&name);
  symbolP = symbol_find_or_make (name);
  S_SET_WEAK (symbolP);
  (void) restore_line_pointer (c);

  SKIP_WHITESPACE ();

  if (!is_end_of_line[(unsigned char) *input_line_pointer])
    {
      if (S_IS_DEFINED (symbolP))
	{
	  as_bad ("Ignoring attempt to redefine symbol `%s'.",
		  S_GET_NAME (symbolP));
	  ignore_rest_of_line ();
	  return;
	}

      if (*input_line_pointer == ',')
	{
	  ++input_line_pointer;
	  SKIP_WHITESPACE ();
	}

      expression (&exp);
      if (exp.X_op != O_symbol)
	{
	  as_bad ("bad .weakext directive");
	  ignore_rest_of_line ();
	  return;
	}
      symbol_set_value_expression (symbolP, &exp);
    }

  demand_empty_rest_of_line ();
}

/* This table describes all the machine specific pseudo-ops the assembler
   has to support.  The fields are:
   Pseudo-op name without dot
   Function to call to execute this pseudo-op
   Integer arg to pass to the function.  */
/* If the pseudo-op is not found in this table, it searches in the obj-elf.c,
   and then in the read.c table.  */
const pseudo_typeS md_pseudo_table[] =
{
  {"lcomm", kpu_s_lcomm, 1},
  {"data", kpu_s_data, 0},
  {"data8", cons, 1},      /* Same as byte.  */
  {"data16", cons, 2},     /* Same as hword.  */
  {"data32", cons, 4},     /* Same as word.  */
  {"ent", s_func, 0}, /* Treat ent as function entry point.  */
  {"end", kpu_s_func, 1}, /* Treat end as function end point.  */
  {"gpword", s_rva, 4}, /* gpword label => store resolved label address in data section.  */
  {"weakext", kpu_s_weakext, 0},
  {"rodata", kpu_s_rdata, 0},
  {"sdata2", kpu_s_rdata, 1},
  {"sdata", kpu_s_sdata, 0},
  {"bss", kpu_s_bss, 0},
  {"sbss", kpu_s_bss, 1},
  {"text", kpu_s_text, 0},
  {"word", cons, 4},
  {"frame", s_ignore, 0},
  {"mask", s_ignore, 0}, /* Emitted by gcc.  */
  {NULL, NULL, 0}
};

/* This function is called once, at assembler startup time.  This should
   set up all the tables, etc that the MD part of the assembler needs.  */

void
md_begin (void)
{
  struct op_code_struct * opcode;

  opcode_hash_control = hash_new ();

  /* Insert unique names into hash table.  */
  for (opcode = opcodes; opcode->name; opcode ++)
    hash_insert (opcode_hash_control, opcode->name, (char *) opcode);
}

/* Symbol modifiers (@GOT, @PLT, @GOTOFF).  */
#define IMM_NONE   0
#define IMM_GOT    1
#define IMM_PLT    2
#define IMM_GOTOFF 3
#define IMM_TLSGD  4
#define IMM_TLSLD  5
#define IMM_TLSDTPMOD 6
#define IMM_TLSDTPREL 7
#define IMM_TLSTPREL  8
#define IMM_MAX    9

struct imm_type {
	const char *isuffix;	 /* Suffix String */
	int itype;       /* Suffix Type */
	int otype;       /* Offset Type */
};

static symbolS * GOT_symbol;

#define GOT_SYMBOL_NAME "_GLOBAL_OFFSET_TABLE_"

static char *
check_got (int * got_type, int * got_len)
{
  char *new_pointer;
  char *atp;
  char *past_got;
  int first, second;
  char *tmpbuf;

  /* Find the start of "@GOT" or "@PLT" suffix (if any).  */
  for (atp = input_line_pointer; *atp != '@'; atp++)
    if (is_end_of_line[(unsigned char) *atp])
      return NULL;

  if (strncmp (atp + 1, "GOTOFF", 5) == 0)
    {
      *got_len = 6;
      *got_type = IMM_GOTOFF;
    }
  else if (strncmp (atp + 1, "GOT", 3) == 0)
    {
      *got_len = 3;
      *got_type = IMM_GOT;
    }
  else if (strncmp (atp + 1, "PLT", 3) == 0)
    {
      *got_len = 3;
      *got_type = IMM_PLT;
    }
  else
    return NULL;

  if (!GOT_symbol)
    GOT_symbol = symbol_find_or_make (GOT_SYMBOL_NAME);

  first = atp - input_line_pointer;

  past_got = atp + *got_len + 1;
  for (new_pointer = past_got; !is_end_of_line[(unsigned char) *new_pointer++];)
    ;
  second = new_pointer - past_got;
  /* One extra byte for ' ' and one for NUL.  */
  tmpbuf = XNEWVEC (char, first + second + 2);
  memcpy (tmpbuf, input_line_pointer, first);
  tmpbuf[first] = ' '; /* @GOTOFF is replaced with a single space.  */
  memcpy (tmpbuf + first + 1, past_got, second);
  tmpbuf[first + second + 1] = '\0';

  return tmpbuf;
}

extern bfd_reloc_code_real_type
parse_cons_expression_kpu (expressionS *exp, int size)
{
  if (size == 4)
    {
      /* Handle @GOTOFF et.al.  */
      char *save, *gotfree_copy;
      int got_len, got_type;

      save = input_line_pointer;
      gotfree_copy = check_got (& got_type, & got_len);
      if (gotfree_copy)
        input_line_pointer = gotfree_copy;

      expression (exp);

      if (gotfree_copy)
	{
          exp->X_md = got_type;
          input_line_pointer = save + (input_line_pointer - gotfree_copy)
	    + got_len;
          free (gotfree_copy);
        }
    }
  else
    expression (exp);
  return BFD_RELOC_NONE;
}

static unsigned int
encode_immediate(unsigned int imm, struct op_code_struct *op)
{

  if (((imm >> op->imm_align) << op->imm_align) != imm)
    as_bad (_("Unaligned access.  Assembler error....."));
  imm = imm >> op->imm_align;
  if (op->inst_offset_type)
    imm--; /* PC is one cycle delayed */

  return imm & op->imm_mask;
}

/* Parse an expression possibly containing a symbol. */

static unsigned int
parse_exp(char *s, char **output, struct op_code_struct *op)
{
  expressionS exp;
  exp.X_md = 0;
  input_line_pointer = s;
  expression (&exp);
  switch (exp.X_op) {
  case O_symbol:
    *output = frag_var(rs_machine_dependent,
		       0x4,
		       0x4,
		       op->reloc_type,
		       exp.X_add_symbol,
		       exp.X_add_number,
		       (char *) 0x0);
    return 0;
  case O_constant:
    *output = frag_more(INST_WORD_SIZE);
    /* FIXME: check for immediate overflow. */
    return encode_immediate(exp.X_add_number, op);
  case O_absent:
    as_fatal (_("missing operand"));
    return 0;
  default:
    as_fatal (_("garbage encurred"));
    return 0;
  }
}

/* Try to parse a reg name.  */

static char *
parse_reg (char *s, unsigned *reg)
{
  unsigned tmpreg = 0;

  if (strncasecmp (s, "pc", 2) == 0) {
    *reg = REG_PC;
    return s + 3;
  }
  else if (strncasecmp (s, "sr", 2) == 0) {
    *reg = REG_SR;
    return s + 3;
  }
  else if (strncasecmp (s, "sp", 2) == 0) {
    *reg = REG_SP;
    return s + 3;
  }
  else if (strncasecmp (s, "fp", 2) == 0) {
    *reg = REG_FP;
    return s + 3;
  }
  else if (TOLOWER (s[0]) == 'r')
    {
      if (ISDIGIT (s[1]) && ISDIGIT (s[2]))
        {
          tmpreg = (s[1] - '0') * 10 + s[2] - '0';
          s += 3;
        }
      else if (ISDIGIT (s[1]))
        {
          tmpreg = s[1] - '0';
          s += 2;
        }
      else
        as_fatal (_("register expected, but saw '%.6s'"), s);

      if ((int)tmpreg >= MIN_REGNUM && tmpreg <= MAX_REGNUM)
        *reg = tmpreg;
      else
        {
          as_fatal (_("Invalid register number at '%.6s'"), s);
          *reg = 0;
        }
      return ++s;
    }

  as_fatal (_("register expected, but saw '%.6s'"), s);
  *reg = 0;
  return ++s;
}

void
md_assemble (char * str)
{
  unsigned int inst;
  char name[20];
  char *p;
  struct op_code_struct *op;
  char * output = NULL;
  int nlen = 0;
  unsigned int ra = 0;
  unsigned int rb = 0;
  unsigned int rc = 0;


  p = str;
  while (*p != ' ' && !is_end_of_line[(unsigned char) *p]) {
    name[nlen] = *p;
    p++;
    nlen++;
  }


  name [nlen] = 0;
  p++;

  if (nlen == 0) {
    as_bad (_("can't find opcode "));
    return;
  }

  op = (struct op_code_struct *) hash_find (opcode_hash_control, name);
  if (op == NULL) {
    as_bad (_("unknown opcode \"%s\""), name);
    return;
  }

  inst = op->bit_sequence;

  switch (op->inst_type) {
  case INST_TYPE_NOP:
    output = frag_more(INST_WORD_SIZE);
    break;
  case INST_TYPE_R:
    output = frag_more(INST_WORD_SIZE);
    p = parse_reg(p, &ra);
    break;
  case INST_TYPE_RR:
    output = frag_more(INST_WORD_SIZE);
    p = parse_reg(p, &ra);
    p = parse_reg(p, &rb);
    if (streq(op->name, "cmp")) {
      rc = rb;
      rb = ra;
      ra = 0;
    }
    break;
  case INST_TYPE_RRR:
    output = frag_more(INST_WORD_SIZE);
    p = parse_reg(p, &ra);
    p = parse_reg(p, &rb);
    p = parse_reg(p, &rc);
    break;
  case INST_TYPE_RRI:
    p = parse_reg(p, &ra);
    p = parse_reg(p, &rb);
    inst |= parse_exp(p, &output, op);
    break;
  case INST_TYPE_RI:
    p = parse_reg(p, &ra);
    if (streq(op->name, "cmpi")) {
      rb = ra;
      ra = 0;
    }
    inst |= parse_exp(p, &output, op);
    break;
  case INST_TYPE_I:
    inst |= parse_exp(p, &output, op);
    break;
  case INST_TYPE_BRK:
    output = frag_more(INST_WORD_SIZE);
    break;
  default:
    as_fatal (_("unknown type for opcode \"%s\""), name);
      break;
  }

  inst |= (ra << RA_LOW) | (rb << RB_LOW) | (rc << RC_LOW);

  output[0] = INST_BYTE0 (inst);
  output[1] = INST_BYTE1 (inst);
  output[2] = INST_BYTE2 (inst);
  output[3] = INST_BYTE3 (inst);

#ifdef OBJ_ELF
  dwarf2_emit_insn (INST_WORD_SIZE);
#endif


}

symbolS *
md_undefined_symbol (char * name ATTRIBUTE_UNUSED)
{
  return NULL;
}

/* Various routines to kill one day.  */
/* Equal to MAX_PRECISION in atof-ieee.c */
#define MAX_LITTLENUMS 6

/* Turn a string in input_line_pointer into a floating point constant of type
   type, and store the appropriate bytes in *litP.  The number of LITTLENUMS
   emitted is stored in *sizeP.  An error message is returned, or NULL on OK.*/
const char *
md_atof (int type, char * litP, int * sizeP)
{
  int prec;
  LITTLENUM_TYPE words[MAX_LITTLENUMS];
  int    i;
  char * t;

  switch (type)
    {
    case 'f':
    case 'F':
    case 's':
    case 'S':
      prec = 2;
      break;

    case 'd':
    case 'D':
    case 'r':
    case 'R':
      prec = 4;
      break;

    case 'x':
    case 'X':
      prec = 6;
      break;

    case 'p':
    case 'P':
      prec = 6;
      break;

    default:
      *sizeP = 0;
      return _("Bad call to MD_NTOF()");
    }

  t = atof_ieee (input_line_pointer, type, words);

  if (t)
    input_line_pointer = t;

  *sizeP = prec * sizeof (LITTLENUM_TYPE);

  if (! target_big_endian)
    {
      for (i = prec - 1; i >= 0; i--)
        {
          md_number_to_chars (litP, (valueT) words[i],
                              sizeof (LITTLENUM_TYPE));
          litP += sizeof (LITTLENUM_TYPE);
        }
    }
  else
    for (i = 0; i < prec; i++)
      {
        md_number_to_chars (litP, (valueT) words[i],
                            sizeof (LITTLENUM_TYPE));
        litP += sizeof (LITTLENUM_TYPE);
      }

  return NULL;
}

const char * md_shortopts = "";

struct option md_longopts[] =
{
  {"EB", no_argument, NULL, OPTION_EB},
  {"EL", no_argument, NULL, OPTION_EL},
  { NULL,          no_argument, NULL, 0}
};

size_t md_longopts_size = sizeof (md_longopts);

int md_short_jump_size;

void
md_create_short_jump (char * ptr ATTRIBUTE_UNUSED,
		      addressT from_Nddr ATTRIBUTE_UNUSED,
		      addressT to_Nddr ATTRIBUTE_UNUSED,
		      fragS * frag ATTRIBUTE_UNUSED,
		      symbolS * to_symbol ATTRIBUTE_UNUSED)
{
  as_fatal (_("failed sanity check: short_jump"));
}

void
md_create_long_jump (char * ptr ATTRIBUTE_UNUSED,
		     addressT from_Nddr ATTRIBUTE_UNUSED,
		     addressT to_Nddr ATTRIBUTE_UNUSED,
		     fragS * frag ATTRIBUTE_UNUSED,
		     symbolS * to_symbol ATTRIBUTE_UNUSED)
{
  as_fatal (_("failed sanity check: long_jump"));
}

/* Called after relaxing, change the frags so they know how big they are.  */

void
md_convert_frag (bfd * abfd ATTRIBUTE_UNUSED,
	         segT sec ATTRIBUTE_UNUSED,
		 fragS * fragP)
{
  if (!fragP->fr_subtype)
    abort();


  fix_new (fragP, fragP->fr_fix, INST_WORD_SIZE, fragP->fr_symbol,
           fragP->fr_offset,
           fragP->fr_subtype == BFD_RELOC_KPU_26_PCREL ? 1 : 0, /*pc rel?*/
           fragP->fr_subtype);
  fragP->fr_fix += INST_WORD_SIZE;
  fragP->fr_var = 0;
}

/* Applies the desired value to the specified location.
   Also sets up addends for 'rela' type relocations.  */
void
md_apply_fix (fixS *   fixP,
	      valueT * valp,
	      segT     segment)
{
  struct op_code_struct *op;
  char *       buf  = fixP->fx_where + fixP->fx_frag->fr_literal;
  unsigned int inst = endianness_fix(*(int32_t *)buf);

  /* Note: use offsetT because it is signed, valueT is unsigned.  */
  offsetT      val  = (offsetT) *valp;

  /* fixP->fx_offset is supposed to be set up correctly for all
     symbol relocations.  */
  if (fixP->fx_addsy == NULL)
    {
      if (!fixP->fx_pcrel)
        fixP->fx_offset = val; /* Absolute relocation.  */
      else
        fprintf (stderr, "NULL symbol PC-relative relocation? offset = %08x, val = %08x\n",
                 (unsigned int) fixP->fx_offset, (unsigned int) val);
    }

  /* If we aren't adjusting this fixup to be against the section
     symbol, we need to adjust the value.  */
  if (fixP->fx_addsy != NULL)
    {
      if (S_IS_WEAK (fixP->fx_addsy)
	  || (symbol_used_in_reloc_p (fixP->fx_addsy)
	      && (((bfd_get_section_flags (stdoutput,
					   S_GET_SEGMENT (fixP->fx_addsy))
		    & SEC_LINK_ONCE) != 0)
		  || !strncmp (segment_name (S_GET_SEGMENT (fixP->fx_addsy)),
			       ".gnu.linkonce",
			       sizeof (".gnu.linkonce") - 1))))
	{
	  val -= S_GET_VALUE (fixP->fx_addsy);
	  if (val != 0 && ! fixP->fx_pcrel)
            {
              /* In this case, the bfd_install_relocation routine will
                 incorrectly add the symbol value back in.  We just want
                 the addend to appear in the object file.
	         FIXME: If this makes VALUE zero, we're toast.  */
              val -= S_GET_VALUE (fixP->fx_addsy);
            }
	}
    }

  /* If the fix is relative to a symbol which is not defined, or not
     in the same segment as the fix, we cannot resolve it here.  */
  /* fixP->fx_addsy is NULL if valp contains the entire relocation.  */
  if (fixP->fx_addsy != NULL
      && (!S_IS_DEFINED (fixP->fx_addsy)
          || (S_GET_SEGMENT (fixP->fx_addsy) != segment)))
    {
      fixP->fx_done = 0;
#ifdef OBJ_ELF
      /* For ELF we can just return and let the reloc that will be generated
         take care of everything.  For COFF we still have to insert 'val'
         into the insn since the addend field will be ignored.  */
      /* return; */
#endif
    }
  /* All fixups in the text section must be handled in the linker.  */
  else if (segment->flags & SEC_CODE)
    fixP->fx_done = 0;
  else if (!fixP->fx_pcrel && fixP->fx_addsy != NULL)
    fixP->fx_done = 0;
  else
    fixP->fx_done = 1;

  if (!fixP->fx_r_type)
    as_bad (_("Relocation not supported.  Assembler error....."));
  else {
    op = kpu_get_op(inst);
    inst |= encode_immediate(val, op);
    inst = endianness_fix(inst);
    *(int32_t *)buf = inst;
  }


  if (fixP->fx_addsy == NULL)
    {
      fixP->fx_addsy = section_symbol (absolute_section);
    }
  return;
}

void
md_operand (expressionS * expressionP)
{
  /* Ignore leading hash symbol, if present.  */
  if (*input_line_pointer == '#')
    {
      input_line_pointer ++;
      expression (expressionP);
    }
}

/* Called just before address relaxation, return the length
   by which a fragment must grow to reach it's destination.  */

int
md_estimate_size_before_relax (fragS * fragP,
			       segT segment_type)
{
  fragP = fragP;
  segment_type = segment_type;
  return INST_WORD_SIZE;
}

/* Put number into target byte order.  */

void
md_number_to_chars (char * ptr, valueT use, int nbytes)
{
  if (target_big_endian)
    number_to_chars_bigendian (ptr, use, nbytes);
  else
    number_to_chars_littleendian (ptr, use, nbytes);
}

/* Round up a section size to the appropriate boundary.  */

valueT
md_section_align (segT segment ATTRIBUTE_UNUSED, valueT size)
{
  return size;			/* Byte alignment is fine.  */
}


/* The location from which a PC relative jump should be calculated,
   given a PC relative reloc.  */

long
md_pcrel_from_section (fixS * fixp, segT sec ATTRIBUTE_UNUSED)
{
#ifdef OBJ_ELF
  /* If the symbol is undefined or defined in another section
     we leave the add number alone for the linker to fix it later.
     Only account for the PC pre-bump (No PC-pre-bump on the Kpu). */

  if (fixp->fx_addsy != (symbolS *) NULL
      && (!S_IS_DEFINED (fixp->fx_addsy)
          || (S_GET_SEGMENT (fixp->fx_addsy) != sec)))
    return 0;
  else
    {
      /* The case where we are going to resolve things... */
      if (fixp->fx_r_type == BFD_RELOC_64_PCREL)
        return  fixp->fx_where + fixp->fx_frag->fr_address + INST_WORD_SIZE;
      else
        return  fixp->fx_where + fixp->fx_frag->fr_address;
    }
#endif
}


#define F(SZ,PCREL)		(((SZ) << 1) + (PCREL))
#define MAP(SZ,PCREL,TYPE)	case F (SZ, PCREL): code = (TYPE); break

arelent *
tc_gen_reloc (asection * section ATTRIBUTE_UNUSED, fixS * fixp)
{
  arelent * rel;
  bfd_reloc_code_real_type code;

  switch (fixp->fx_r_type)
    {
    case BFD_RELOC_NONE:
    case BFD_RELOC_32:
    case BFD_RELOC_RVA:
    case BFD_RELOC_64:
    case BFD_RELOC_KPU_16_NA:
    case BFD_RELOC_KPU_16_HA:
    case BFD_RELOC_KPU_16_WA:
    case BFD_RELOC_KPU_21_NA:
    case BFD_RELOC_KPU_21_WA:
    case BFD_RELOC_KPU_26_PCREL:
      code = fixp->fx_r_type;
      break;

    default:
      switch (F (fixp->fx_size, fixp->fx_pcrel))
        {
          MAP (1, 0, BFD_RELOC_8);
          MAP (2, 0, BFD_RELOC_16);
          MAP (4, 0, BFD_RELOC_32);
          MAP (1, 1, BFD_RELOC_8_PCREL);
          MAP (2, 1, BFD_RELOC_16_PCREL);
          MAP (4, 1, BFD_RELOC_32_PCREL);
        default:
          code = fixp->fx_r_type;
          as_bad (_("Can not do %d byte %srelocation"),
                  fixp->fx_size,
                  fixp->fx_pcrel ? _("pc-relative ") : "");
        }
      break;
    }

  rel = XNEW (arelent);
  rel->sym_ptr_ptr = XNEW (asymbol *);

  *rel->sym_ptr_ptr = symbol_get_bfdsym (fixp->fx_addsy);

  rel->address = fixp->fx_frag->fr_address + fixp->fx_where;
  /* Always pass the addend along!  */
  rel->addend = fixp->fx_offset;
  rel->howto = bfd_reloc_type_lookup (stdoutput, code);

  if (rel->howto == NULL)
    {
      as_bad_where (fixp->fx_file, fixp->fx_line,
                    _("Cannot represent relocation type %s"),
                    bfd_get_reloc_code_name (code));

      /* Set howto to a garbage value so that we can keep going.  */
      rel->howto = bfd_reloc_type_lookup (stdoutput, BFD_RELOC_32);
      gas_assert (rel->howto != NULL);
    }
  return rel;
}

int
md_parse_option (int c, const char * arg ATTRIBUTE_UNUSED)
{
  switch (c)
    {
    case OPTION_EB:
      target_big_endian = 1;
      break;
    case OPTION_EL:
      target_big_endian = 0;
      break;
    default:
      return 0;
    }
  return 1;
}

void
md_show_usage (FILE * stream ATTRIBUTE_UNUSED)
{
  /*  fprintf(stream, _("\
      Kpu options:\n\
      -noSmall         Data in the comm and data sections do not go into the small data section\n")); */
}


/* Create a fixup for a cons expression.  If parse_cons_expression_kpu
   found a machine specific op in an expression,
   then we create relocs accordingly.  */

void
cons_fix_new_kpu (fragS * frag,
			 int where,
			 int size,
			 expressionS *exp,
			 bfd_reloc_code_real_type r)
{
  switch (size)
    {
    case 1:
      r = BFD_RELOC_8;
      break;
    case 2:
      r = BFD_RELOC_16;
      break;
    case 4:
      r = BFD_RELOC_32;
      break;
    case 8:
      r = BFD_RELOC_64;
      break;
    default:
      as_bad (_("unsupported BFD relocation size %u"), size);
      r = BFD_RELOC_32;
      break;
    }

  fix_new_exp (frag, where, size, exp, 0, r);
}
