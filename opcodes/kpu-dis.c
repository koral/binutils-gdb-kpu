/* Disassemble KPU instructions.

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING.  If not, write to the
   Free Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */


#include "sysdep.h"
#define STATIC_TABLE
#define DEFINE_TABLE

#include "dis-asm.h"
#include <strings.h>
#include "kpu-opc.h"
#include "kpu-dis.h"

struct op_code_struct opcodes[MAX_OPCODES] =
{
  {"ldw",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0x04000000, ldw_i,  data_i_t, BFD_RELOC_KPU_16_WA },
  {"stw",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0x08000000, stw_i,  data_i_t, BFD_RELOC_KPU_16_WA },
  {"ldh",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_H_ALIGN,  IMM_SIGNED,   0x0c000000, ldh_i,  data_i_t, BFD_RELOC_KPU_16_HA },
  {"sth",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_H_ALIGN,  IMM_SIGNED,   0x10000000, sth_i,  data_i_t, BFD_RELOC_KPU_16_HA },
  {"ldhu",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_H_ALIGN,  IMM_UNSIGNED, 0x14000000, ldhu_i, data_i_t, BFD_RELOC_KPU_16_HA },
  {"sthu",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_H_ALIGN,  IMM_UNSIGNED, 0x18000000, sthu_i, data_i_t, BFD_RELOC_KPU_16_HA },
  {"ldb",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_B_ALIGN,  IMM_SIGNED,   0x1c000000, ldb_i,  data_i_t, BFD_RELOC_KPU_16_NA },
  {"stb",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_B_ALIGN,  IMM_SIGNED,   0x20000000, stb_i,  data_i_t, BFD_RELOC_KPU_16_NA },
  {"ldbu",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_B_ALIGN,  IMM_UNSIGNED, 0x24000000, ldbu_i, data_i_t, BFD_RELOC_KPU_16_NA },
  {"stbu",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_B_ALIGN,  IMM_UNSIGNED, 0x28000000, stbu_i, data_i_t, BFD_RELOC_KPU_16_NA },
  {"ldaw",  INST_TYPE_RI,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM21_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0x2c000000, ldaw_i, data_i_t, BFD_RELOC_KPU_21_WA },
  {"staw",  INST_TYPE_RI,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM21_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0x30000000, staw_i, data_i_t, BFD_RELOC_KPU_21_WA },
  {"mov",   INST_TYPE_RR,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x34000000, mov_i,  mov_i_t, BFD_RELOC_NONE },
  {"movi",  INST_TYPE_RI,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM21_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x3c000000, movi_i, mov_i_t, BFD_RELOC_KPU_21_NA },

  {"add",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x40000000, add_i,  alu_i_t, BFD_RELOC_NONE },
  {"sub",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x44000000, sub_i,  alu_i_t, BFD_RELOC_NONE },
  {"shr",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x48000000, shr_i,  alu_i_t, BFD_RELOC_NONE },
  {"shl",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x4c000000, shl_i,  alu_i_t, BFD_RELOC_NONE },
  {"not",   INST_TYPE_RR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x50000000, not_i,  alu_i_t, BFD_RELOC_NONE },
  {"and",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x54000000, and_i,  alu_i_t, BFD_RELOC_NONE },
  {"or",    INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x58000000, or_i,   alu_i_t, BFD_RELOC_NONE },
  {"xor",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x5c000000, xor_i,  alu_i_t, BFD_RELOC_NONE },
  {"mult",  INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x60000000, mult_i, alu_i_t, BFD_RELOC_NONE },
  {"div",   INST_TYPE_RRR, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x64000000, div_i,  alu_i_t, BFD_RELOC_NONE },
  {"cmpu",  INST_TYPE_RR,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x78000000, cmpu_i, alu_i_t, BFD_RELOC_NONE },
  {"cmp",   INST_TYPE_RR,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, 0x7c000000, cmp_i,  alu_i_t, BFD_RELOC_NONE },

  {"addi",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x80000000, addi_i,  alu_i_t, BFD_RELOC_NONE },
  {"subi",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x84000000, subi_i,  alu_i_t, BFD_RELOC_NONE },
  {"shri",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x88000000, shri_i,  alu_i_t, BFD_RELOC_NONE },
  {"shli",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x8c000000, shli_i,  alu_i_t, BFD_RELOC_NONE },
  {"andi",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x94000000, andi_i,  alu_i_t, BFD_RELOC_NONE },
  {"ori",   INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x98000000, ori_i,   alu_i_t, BFD_RELOC_NONE },
  {"xori",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0x9c000000, xori_i,  alu_i_t, BFD_RELOC_NONE },
  {"multi", INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0xa0000000, multi_i, alu_i_t, BFD_RELOC_NONE },
  {"divi",  INST_TYPE_RRI, INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0xa4000000, divi_i,  alu_i_t, BFD_RELOC_NONE },
  {"cmpui", INST_TYPE_RI,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0xb8000000, cmpui_i, alu_i_t, BFD_RELOC_NONE },
  {"cmpi",  INST_TYPE_RI,  INST_NO_PC_REL, NO_DELAY_SLOT, IMM16_MASK,  IMM_NO_ALIGN, IMM_SIGNED,   0xbc000000, cmpi_i,  alu_i_t, BFD_RELOC_NONE },

  {"jmp",   INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xc0000000, jmp_i,  branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bo",    INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xc4000000, bo_i,   branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bg",    INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xc8000000, bg_i,   branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bl",    INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xcc000000, bl_i,   branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"beq",   INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xd0000000, beq_i,  branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"beg",   INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xd8000000, beg_i,  branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bel",   INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xdc000000, bel_i,  branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bgz",   INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xe0000000, bgz_i,  branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"bneq",  INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xe4000000, bneq_i, branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"jmpr",  INST_TYPE_R,   INST_NO_PC_REL, DELAY_SLOT,    IMM_NO_MASK, IMM_NO_ALIGN, IMM_SIGNED,   0xf4000000, jmpr_i, branch_i_t, BFD_RELOC_NONE },
  {"call",  INST_TYPE_I,   INST_PC_REL,    DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xf8000000, call_i, branch_i_t, BFD_RELOC_KPU_26_PCREL },
  {"int",   INST_TYPE_I,   INST_NO_PC_REL, DELAY_SLOT,    IMM26_MASK,  IMM_W_ALIGN,  IMM_SIGNED,   0xfc000000, int_i,  branch_i_t, BFD_RELOC_NONE },

  {"brk",   INST_TYPE_BRK, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, BRK_BITSEQ, brk_i,  other_i_t, BFD_RELOC_NONE },
  {"nop",   INST_TYPE_NOP, INST_NO_PC_REL, NO_DELAY_SLOT, IMM_NO_MASK, IMM_NO_ALIGN, IMM_UNSIGNED, NOP_BITSEQ, nop_i,  other_i_t, BFD_RELOC_NONE },

  {"", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

static const char *const reg_names[] = {
  "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9",
  "r10", "r11", "r12", "r13", "r14", "r15", "r16", "r17",
  "r18", "r19", "r20", "r21", "r22", "r23", "r24", "r25",
  "fp", "sp", "r28", "r29", "st", "pc" };

static unsigned long
read_insn_kpu (bfd_vma memaddr,
	       struct disassemble_info *info,
	       struct op_code_struct **opr)
{
  unsigned char       ibytes[4];
  int                 status;
  struct op_code_struct * op;
  unsigned long inst;

  status = info->read_memory_func (memaddr, ibytes, 4, info);

  if (status != 0)
    {
      info->memory_error_func (status, memaddr, info);

      return -1;
    }

  if (info->endian == BFD_ENDIAN_BIG)
    inst = (ibytes[0] << 24) | (ibytes[1] << 16) | (ibytes[2] << 8) | ibytes[3];
  else if (info->endian == BFD_ENDIAN_LITTLE)
    inst = (ibytes[3] << 24) | (ibytes[2] << 16) | (ibytes[1] << 8) | ibytes[0];
  else
    abort ();

  op = kpu_get_op(inst);

  *opr = op;
  return inst;
}

static int
decode_immediate(unsigned int insn, unsigned int imm_mask, short imm_align,
                 short pc_rel, short is_signed)
{
  int imm;

  imm = (insn & imm_mask) >> IMM_LOW;

  if (is_signed && imm >>  (__builtin_popcount(imm_mask) - 1))
    imm |= ~imm_mask;
  if (pc_rel)
    imm++;

  return imm << imm_align;
}

struct op_code_struct *
kpu_get_op (unsigned int inst)
{
  struct op_code_struct *op;

  /* Just a linear search of the table.  */
  for (op = opcodes; op->name != 0; op ++)
    if (op->bit_sequence == (inst & OP_MASK))
      break;
  return op;
}

enum kpu_instr
kpu_decode_insn (struct op_code_struct * op, long insn,
		 int *ra, int *rb, int *rc, int *immed)
{
  enum kpu_instr op_i;
  bfd_boolean is_unsigned, pc_rel;
  enum kpu_instr_type t2;
  short imm_align, t3;
  unsigned imm_mask;

  op_i = get_insn_kpu (op, insn, &pc_rel, &is_unsigned, &t2, &imm_mask,
		       &imm_align, &t3);
  *ra = (insn & RA_MASK) >> RA_LOW;
  *rb = (insn & RB_MASK) >> RB_LOW;
  *rc = (insn & RC_MASK) >> RC_LOW;

  *immed = decode_immediate(insn, imm_mask, imm_align, pc_rel, !is_unsigned);

  return op_i;
}


enum kpu_instr
get_insn_kpu (struct op_code_struct * op,
	      unsigned inst,
	      bfd_boolean *pc_rel,
              bfd_boolean *isunsignedimm,
              enum kpu_instr_type *insn_type,
              unsigned *imm_mask,
	      short *imm_align,
              short *delay_slots)
{
  *isunsignedimm = FALSE;

  if (!op)
      op = kpu_get_op(inst);

  if (op->name == 0)
    return invalid_inst;
  else
    {
      *pc_rel = op->inst_offset_type;
      *isunsignedimm = !op->imm_signed;
      *insn_type = op->instr_type;
      *delay_slots = op->delay_slots;
      *imm_mask = op->imm_mask;
      *imm_align = op->imm_align;
      return op->inst;
    }
}

int
print_insn_kpu (bfd_vma memaddr, struct disassemble_info * info)
{
  fprintf_ftype       print_func = info->fprintf_func;
  void *              stream = info->stream;
  unsigned long       inst;
  struct op_code_struct * op;

  info->bytes_per_chunk = 4;

  inst = read_insn_kpu (memaddr, info, &op);

  if (inst == -1UL)
    return 4;

  if (inst == NOP_BITSEQ)
    print_func (stream, "nop");
  else if (op->name == NULL)
    print_func (stream, ".word 0x%04x", (unsigned int) inst);
  else
    {
      int ra, rb, rc;
      int imm;
      kpu_decode_insn(op, inst, &ra, &rb, &rc, &imm);
      print_func (stream, "%s", op->name);

      switch (op->inst_type)
	{
	case INST_TYPE_R:
	  print_func (stream, "\t %s", reg_names[ra]);
	  break;
	case INST_TYPE_RR:
	  print_func (stream, "\t %s, %s", reg_names[ra], reg_names[rb]);
	  break;
	case INST_TYPE_RRR:
	  print_func (stream, "\t %s, %s, %s", reg_names[ra], reg_names[rb],
                      reg_names[rc]);
	  break;
	case INST_TYPE_RRI:
          if (op->inst_offset_type == INST_PC_REL)
            print_func (stream, "\t %s, %s, %d", reg_names[ra], reg_names[rb],
                        imm);
          else
            print_func (stream, "\t %s, %s, 0x%x", reg_names[ra],
                        reg_names[rb], imm);
	  break;
	case INST_TYPE_RI:
          if (op->inst_offset_type == INST_PC_REL)
            print_func (stream, "\t %s, %d", reg_names[ra], imm);
          else
            print_func (stream, "\t %s, 0x%x", reg_names[ra], imm);
	  break;
	case INST_TYPE_I:
          if (op->inst_offset_type == INST_PC_REL)
            print_func (stream, "\t %d", imm);
          else
            print_func (stream, "\t 0x%x", imm);
	  break;
	default:
	  break;
	}

      /* add some immediate information if possible */
      switch (op->inst_type)
	{
	case INST_TYPE_RRI:
	case INST_TYPE_RI:
	case INST_TYPE_I:
	  if (op->inst_offset_type == INST_PC_REL)
	    imm += (int) memaddr;
	  if (info->symbol_at_address_func ((unsigned)imm, info))
	    {
	      print_func (stream, "\t\t; ");
	      info->print_address_func (imm, info);
	    }
	  else if (op->inst_offset_type == INST_PC_REL)
	    {
	      print_func (stream, "\t\t\t; ");
	      print_func (stream, "0x%x", imm);
	    }
	default:
	  break;
	}

    }
  /* Say how many bytes we consumed.  */
  return 4;
}
