/* Disassemble KPU instructions.

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING.  If not, write to the
   Free Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#ifndef KPU_DIS_H
#define KPU_DIS_H 1

#include "kpu-opc.h"

#ifdef __cplusplus
extern "C" {
#endif

  extern enum kpu_instr kpu_decode_insn (struct op_code_struct *,long, int *, int *,
                                         int *, int *);
  extern unsigned long kpu_get_target_address (long, bfd_boolean, int,
                                               long, long, long, bfd_boolean *, bfd_boolean *);

  extern enum kpu_instr get_insn_kpu (struct op_code_struct *, unsigned ,
				      bfd_boolean *, bfd_boolean *,
				      enum kpu_instr_type *, unsigned *,
				      short *, short *);
  extern struct op_code_struct *kpu_get_op (unsigned int inst);

#ifdef __cplusplus
}
#endif

#endif /* kpu-dis.h */
