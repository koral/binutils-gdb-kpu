/* kpu-opc.h -- Kpu Opcodes

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING.  If not, write to the
   Free Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */


#ifndef KPU_OPC
#define KPU_OPC

enum kpu_instr
{
  nop_i, ldw_i, stw_i, ldh_i, sth_i, ldhu_i, sthu_i, ldb_i, stb_i, ldbu_i,
  stbu_i, ldaw_i, staw_i, mov_i, movi_i, add_i, sub_i, shr_i, shl_i, not_i,
  and_i, or_i, xor_i, mult_i, div_i, cmpu_i, cmp_i, addi_i, subi_i, shri_i,
  shli_i, andi_i, ori_i, xori_i, multi_i, divi_i, cmpui_i, cmpi_i, jmp_i, bo_i,
  bg_i, bl_i, beq_i, beg_i, bel_i, bgz_i, bneq_i, jmpr_i, call_i, int_i, imm_i,
  brk_i, invalid_inst
};

enum kpu_instr_type
{
  data_i_t, alu_i_t, alui_i_t, branch_i_t, mov_i_t, other_i_t,
};

#define INST_WORD_SIZE 4


#define RA_MASK 0x03E00000
#define RB_MASK 0x001F0000
#define RC_MASK 0x0000F800
#define IMM_MASK 0x0000FFFF

#define INST_TYPE_NOP 0
#define INST_TYPE_R 1
#define INST_TYPE_RR 2
#define INST_TYPE_RRR 3
#define INST_TYPE_RRI 4
#define INST_TYPE_RI 5
#define INST_TYPE_I 6
#define INST_TYPE_BRK 7

/* Instructions where the label address is resolved as a PC offset
   (for branch label).  */
#define INST_PC_REL 1
/* Instructions where the label address is resolved as an absolute
   value (for data mem or abs address).  */
#define INST_NO_PC_REL 0

#define RA_LOW 21 /* Low bit for RA.  */
#define RB_LOW 16 /* Low bit for RB.  */
#define RC_LOW 11 /* Low bit for RC.  */
#define IMM_LOW 0 /* Low bit for IMM.  */

#define IMM26_MASK ((1 << 26) - 1)
#define IMM21_MASK ((1 << 21) - 1)
#define IMM16_MASK ((1 << 16) - 1)
#define IMM_NO_MASK (-1)

#define IMM_NO_ALIGN 0 /* No alignment required.*/
#define IMM_B_ALIGN 0  /* Byte alignment required.*/
#define IMM_H_ALIGN 1  /* Half word alignment required.*/
#define IMM_W_ALIGN 2  /* Word alignment required.*/

#define IMM_UNSIGNED 0
#define IMM_SIGNED 1

#define OP_MASK   0xFC000000  /* High 6 bits only.  */

#ifdef DELAY_SLOT
#undef DELAY_SLOT
#endif

#define DELAY_SLOT 1
#define NO_DELAY_SLOT 0

#define MAX_OPCODES 289

struct op_code_struct
{
  const char * name;
  short inst_type;            /* Registers and immediate values involved.  */
  short inst_offset_type;     /* Immediate vals offset from PC? (= 1 for branches).  */
  short delay_slots;          /* Info about delay slots needed after this instr. */
  unsigned int imm_mask;      /* Immediate mask. */
  short imm_align;            /* Immediate value alignment requirement. */
  short imm_signed;           /* Immediate is signed. */
  unsigned int bit_sequence;  /* All the fixed bits for the op are set and
				 all the variable bits (reg names, imm vals)
				 are set to 0.  */
  enum kpu_instr inst;
  enum kpu_instr_type instr_type;
  int reloc_type;
  /* More info about output format here.  */
};

extern struct op_code_struct opcodes[MAX_OPCODES];

#define BRK_BITSEQ 0x00000000  /* break point */
#define NOP_BITSEQ 0x38000000  /* nop */

/* #defines for valid immediate range.  */
#define MIN_IMM  ((int) 0x80000000)
#define MAX_IMM  ((int) 0x7fffffff)

#define MIN_IMM15 ((int) 0x0000)
#define MAX_IMM15 ((int) 0x7fff)

#define MIN_IMM5  ((int) 0x00000000)
#define MAX_IMM5  ((int) 0x0000001f)

#define MIN_REGNUM 0
#define MAX_REGNUM 31

#define REG_SP  27 /* Stack pointer.  */
#define REG_FP  26 /* Frame ponter.  */

#define REG_PC  31 /* PC.  */
#define REG_SR  30 /* Machine status reg.  */


#endif /* KPU_OPC */
