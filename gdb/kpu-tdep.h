/* Target-dependent code for KPU

   Copyright (C) 2009-2017 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef KPU_TDEP_H
#define KPU_TDEP_H 1


/* Kpu architecture-specific information.  */
struct gdbarch_tdep
{
};

/* Register numbers.  */
enum kpu_regnum
{
  KPU_R0_REGNUM,
  KPU_R1_REGNUM, KPU_SP_REGNUM = KPU_R1_REGNUM,
  KPU_R2_REGNUM,
  KPU_R3_REGNUM, KPU_RETVAL_REGNUM = KPU_R3_REGNUM,
  KPU_R4_REGNUM,
  KPU_R5_REGNUM, KPU_FIRST_ARGREG = KPU_R5_REGNUM,
  KPU_R6_REGNUM,
  KPU_R7_REGNUM,
  KPU_R8_REGNUM,
  KPU_R9_REGNUM,
  KPU_R10_REGNUM, KPU_LAST_ARGREG = KPU_R10_REGNUM,
  KPU_R11_REGNUM,
  KPU_R12_REGNUM,
  KPU_R13_REGNUM,
  KPU_R14_REGNUM,
  KPU_R15_REGNUM,
  KPU_R16_REGNUM,
  KPU_R17_REGNUM,
  KPU_R18_REGNUM,
  KPU_R19_REGNUM,
  KPU_R20_REGNUM,
  KPU_R21_REGNUM,
  KPU_R22_REGNUM,
  KPU_R23_REGNUM,
  KPU_R24_REGNUM,
  KPU_R25_REGNUM,
  KPU_R26_REGNUM,
  KPU_R27_REGNUM,
  KPU_R28_REGNUM,
  KPU_R29_REGNUM,
  KPU_R30_REGNUM, KPU_SR_REGNUM = KPU_R30_REGNUM,
  KPU_R31_REGNUM, KPU_PC_REGNUM = KPU_R31_REGNUM,
  KPU_NUM_REGS
};

struct kpu_frame_cache
{
  /* Base address.  */
  CORE_ADDR base;
  CORE_ADDR pc;

  /* Do we have a frame?  */
  int frameless_p;

  /* Frame size.  */
  int framesize;

  /* Frame register.  */
  int fp_regnum;

  /* Offsets to saved registers.  */
  int register_offsets[KPU_NUM_REGS];

  /* Table of saved registers.  */
  struct trad_frame_saved_reg *saved_regs;
};
/* All registers are 32 bits.  */
#define KPU_REGISTER_SIZE 4

/* KPU_BREAKPOINT defines the breakpoint that should be used.
   Only used for native debugging.  */
#define KPU_BREAKPOINT { 0x00, 0x00, 0x00, 0x00 }

#endif /* kpu-tdep.h */
